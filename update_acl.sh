#!/bin/bash

file_path="/root/acl_ipv6.conf"
line_number_total=$(cat /root/acl_ipv6.conf | wc -l)
file_path_ip="/root/ipv6.txt"

# Vòng lặp for
for ((line_number=1; line_number<=line_number_total; line_number++))
do
  line=$(sed "${line_number}q;d" "$file_path")
  rules=$(sed "${line_number}q;d" "$file_path" | awk '{print $2}')
  ipv6=$(sed "${line_number}q;d" "$file_path_ip")
  rules_proxy="tcp_outgoing_address $ipv6 $rules"

  echo $line >> acl_ipv6.conf.new
  echo $rules_proxy >> acl_ipv6.conf.new
done