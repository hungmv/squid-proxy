#!/bin/bash
############################################################
# Squid Proxy Installer
############################################################

if [ `whoami` != root ]; then
	echo "ERROR: You need to run the script as user root or add sudo before command."
	exit 1
fi

/usr/bin/wget --no-check-certificate -O /usr/local/bin/find-os https://gitlab.com/hungmv/squid-proxy/-/rawgitlab.com/hungmv/squid-proxy/-/raw/master/find-os.sh > /dev/null 2>&1
chmod 755 /usr/local/bin/find-os

/usr/bin/wget --no-check-certificate -O /usr/local/bin/squid-uninstall https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid-uninstall.sh > /dev/null 2>&1
chmod 755 /usr/local/bin/squid-uninstall

/usr/bin/wget --no-check-certificate -O /usr/local/bin/squid-add-user https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid-add-user.sh > /dev/null 2>&1
chmod 755 /usr/local/bin/squid-add-user

if [[ -d /etc/squid/ || -d /etc/squid3/ ]]; then
    echo "Squid Proxy already installed. If you want to reinstall, first uninstall squid proxy by running command: squid-uninstall"
    exit 1
fi
/usr/bin/wget --no-check-certificate -O /usr/local/bin/find-os https://gitlab.com/hungmv/squid-proxy/-/raw/master/find-os.sh >> squid-install.log 2>&1 2>&1
    chmod 755 /usr/local/bin/find-os

    /usr/bin/wget --no-check-certificate -O /usr/local/bin/squid-uninstall https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid-uninstall.sh >> squid-install.log 2>&1 2>&1
    chmod 755 /usr/local/bin/squid-uninstall

    /usr/bin/wget --no-check-certificate -O /usr/local/bin/squid-add-user https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid-add-user.sh >> squid-install.log 2>&1 2>&1 
    chmod 755 /usr/local/bin/squid-add-user 


    if [[ -d /etc/squid/ || -d /etc/squid3/ ]]; then
        echo "Squid Proxy duoc cai dat. Neu ban muon cai dat lại, truoc tien hay go bo squid proxy bang command: squid-uninstall"
        exit 1
    fi

    if cat /etc/os-release | grep PRETTY_NAME | grep "Ubuntu 22.04"; then
        /usr/bin/apt update >> squid-install.log 2>&1
        /usr/bin/apt -y install apache2-utils squid >> squid-install.log 2>&1
        touch /etc/squid/passwd >> squid-install.log 2>&1
        mv /etc/squid/squid.conf /etc/squid/squid.conf.bak  >> squid-install.log 2>&1
        /usr/bin/touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/ubuntu-2204.conf  >> squid-install.log 2>&1
        if [ -f /sbin/iptables ]; then
            /sbin/iptables -I INPUT -p tcp --dport 3128 -j ACCEPT   >> squid-install.log 2>&1
            /sbin/iptables-save >> squid-install.log 2>&1
        fi
        service squid restart   >> squid-install.log 2>&1
        systemctl enable squid  >> squid-install.log 2>&1
    elif cat /etc/os-release | grep PRETTY_NAME | grep "Ubuntu 20.04"; then
        /usr/bin/apt update >> squid-install.log 2>&1
        /usr/bin/apt -y install apache2-utils squid >> squid-install.log 2>&1
        touch /etc/squid/passwd >> squid-install.log 2>&1
        /bin/rm -f /etc/squid/squid.conf >> squid-install.log 2>&1
        /usr/bin/touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid.conf >> squid-install.log 2>&1
        if [ -f /sbin/iptables ]; then
            /sbin/iptables -I INPUT -p tcp --dport 3128 -j ACCEPT   >> squid-install.log 2>&1
            /sbin/iptables-save >> squid-install.log 2>&1
        fi  
        service squid restart   >> squid-install.log 2>&1
        systemctl enable squid  >> squid-install.log 2>&1
    elif cat /etc/os-release | grep PRETTY_NAME | grep "Ubuntu 18.04"; then
        /usr/bin/apt update >> squid-install.log 2>&1
        /usr/bin/apt -y install apache2-utils squid3 >> squid-install.log 2>&1
        touch /etc/squid/passwd >> squid-install.log 2>&1
        /bin/rm -f /etc/squid/squid.conf >> squid-install.log 2>&1
        /usr/bin/touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid.conf >> squid-install.log 2>&1
        /sbin/iptables -I INPUT -p tcp --dport 3128 -j ACCEPT   >> squid-install.log 2>&1
        /sbin/iptables-save >> squid-install.log 2>&1
        service squid restart   >> squid-install.log 2>&1
        systemctl enable squid  >> squid-install.log 2>&1

    elif cat /etc/os-release | grep PRETTY_NAME | grep "jessie"; then
        # OS = Debian 8
        /bin/rm -rf /etc/squid  >> squid-install.log 2>&1
        /usr/bin/apt update >> squid-install.log 2>&1
        /usr/bin/apt -y install apache2-utils squid3 >> squid-install.log 2>&1
        touch /etc/squid3/passwd >> squid-install.log 2>&1
        /bin/rm -f /etc/squid3/squid.conf   >> squid-install.log 2>&1
        /usr/bin/touch /etc/squid3/blacklist.acl >> squid-install.log 2>&1
        /usr/bin/wget --no-check-certificate -O /etc/squid3/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid.conf >> squid-install.log 2>&1
        /sbin/iptables -I INPUT -p tcp --dport 3128 -j ACCEPT >> squid-install.log 2>&1
        /sbin/iptables-save >> squid-install.log 2>&1
        service squid3 restart >> squid-install.log 2>&1
        update-rc.d squid3 defaults >> squid-install.log 2>&1
        ln -s /etc/squid3 /etc/squid >> squid-install.log 2>&1
    elif cat /etc/os-release | grep PRETTY_NAME | grep "stretch"; then
        # OS = Debian 9
        /bin/rm -rf /etc/squid >> squid-install.log 2>&1
        /usr/bin/apt update >> squid-install.log 2>&1
        /usr/bin/apt -y install apache2-utils squid >> squid-install.log 2>&1
        touch /etc/squid/passwd >> squid-install.log 2>&1
        /bin/rm -f /etc/squid/squid.conf >> squid-install.log 2>&1
        /usr/bin/touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid.conf >> squid-install.log 2>&1
        /sbin/iptables -I INPUT -p tcp --dport 3128 -j ACCEPT >> squid-install.log 2>&1
        /sbin/iptables-save >> squid-install.log 2>&1
        systemctl enable squid  >> squid-install.log 2>&1
        systemctl restart squid >> squid-install.log 2>&1
    elif cat /etc/os-release | grep PRETTY_NAME | grep "buster"; then
        # OS = Debian 10
        /bin/rm -rf /etc/squid  >> squid-install.log 2>&1
        /usr/bin/apt update >> squid-install.log 2>&1    
        /usr/bin/apt -y install apache2-utils squid >> squid-install.log 2>&1
        touch /etc/squid/passwd >> squid-install.log 2>&1
        /bin/rm -f /etc/squid/squid.conf >> squid-install.log 2>&1
        /usr/bin/touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid.conf >> squid-install.log 2>&1
        /sbin/iptables -I INPUT -p tcp --dport 3128 -j ACCEPT   >> squid-install.log 2>&1
        /sbin/iptables-save >> squid-install.log 2>&1
        systemctl enable squid  >> squid-install.log 2>&1
        systemctl restart squid >> squid-install.log 2>&1
    elif cat /etc/os-release | grep PRETTY_NAME | grep "CentOS Linux 7"; then
        yum install squid httpd-tools -y >> squid-install.log 2>&1
        /bin/rm -f /etc/squid/squid.conf >> squid-install.log 2>&1
        /usr/bin/touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/squid-centos7.conf >> squid-install.log 2>&1 2>&1
        systemctl enable squid >> squid-install.log 2>&1 2>&1
        systemctl restart squid >> squid-install.log 2>&1
        #firewall-cmd --zone=public --permanent --add-port=3128/tcp
        #firewall-cmd --reload
    elif cat /etc/os-release | grep PRETTY_NAME | grep "CentOS Linux 8"; then
        yum install squid httpd-tools -y >> squid-install.log 2>&1
        /bin/rm -f /etc/squid/squid.conf >> squid-install.log 2>&1
        /usr/bin/touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        /usr/bin/wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/squid-centos7.conf >> squid-install.log 2>&1
        systemctl enable squid >> squid-install.log 2>&1
        systemctl restart squid >> squid-install.log 2>&1
        # firewall-cmd --zone=public --permanent --add-port=3128/tcp
        # firewall-cmd --reload
    else
        echo "Hệ điều hành không được hỗ trợ.\n"
        echo ""
        exit 1;
    fi

    echo
    ## Tạo User
    USERNAME=$(head /dev/urandom | tr -dc a-z0-9 | head -c 8 ; echo '')
    PASSWORD=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 12 ; echo '')
    
    /usr/bin/htpasswd -b -c /etc/squid/passwd $USERNAME $PASSWORD   >> squid-install.log 2>&1
    echo "username      password"
    echo "$USERNAME     $PASSWORD"
    echo "username      password" > user.txt
    echo "$USERNAME     $PASSWORD" >> user.txt
