# Script tự động cài đặt Squid proxy trên VPS của Vinahost
### Nếu bạn chưa có VPS hãy tham khảo các gói VPS của Vinhahost tại đây [VPS SSD](https://vinahost.vn/thue-vps-ssd-chuyen-nghiep/)

## Scrip tự động cài đặt proxy Squid IPv6 trên 3 hệ điều hành linux sau:
* Ubuntu 18.04, 20.04, 22.04
* Debian 8, 9, 10
* CentOS 7, 8

## Hướng dẫn cài đặt:

Để cài đặt, hãy chạy lệnh sau với user root hoặc user có quyền sudo:
```
curl -Ls https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid3-install-ipv6.sh | sudo bash
```
Sau khi cài đặt thành công, thông tin đăng nhập được hiển thị trên màn hình, hoặc xem lại tại tệp user.txt

<img src="./img/script.png" >



## Nếu muốn tạo thêm user, hãy làm theo hướng dẫn dưới đây

Để tạo user, chạy command dưới đây
```
squid-add-user
```

Để cập nhật mật khẩu cho người dùng hiện tại, chạy
```
sudo /usr/bin/htpasswd /etc/squid/passwd USERNAME
```

## Thay đổi cổng mặc định của squid proxy
Bạn có thể sử dụng lệnh sed để thay thế số cổng
```
sed -i 's/^http_port.*$/http_port NEW_PORT_HERE/g'  /etc/squid/squid.conf
```

Hoặc chỉnh sửa tệp cấu hình Squid bằng trình chỉnh sửa vi hoặc nano
```
vi /etc/squid/squid.conf
```
Trong tệp, tìm phần http_port như bên dưới
```
http_port NEW_PORT_HERE
```
Trong hai lệnh trên, thay thế NEW_PORT_HERE bằng số cổng bạn cần.
Ví dụ: để chạy squid proxy trên cổng 3333, hãy chạy
```
sed -i 's/^http_port.*$/http_port 3333/g'  /etc/squid/squid.conf
```

Sau khi thay đổi cổng, bạn cần khởi động lại squid proxy
```
sudo systemctl restart squid
```
Nếu VPS của bạn có tường lửa, bạn cần mở port cho Squid.
Ví dụ với tường lửa UFW của ubuntu
```
sudo ufw allow 3333/tcp
sudo ufw reload
```
Ví dụ với firewalld của centOs
```
firewall-cmd --zone=public --permanent --add-port=3333/tcp
firewall-cmd --reload
```
___Thay thế 3333 thành số cổng squid proxy của bạn.___

 Như vậy là đã xong phần cài đặt Squid Proxy với Script trên VPS của Vinahost. Chúc các bạn thành công!

