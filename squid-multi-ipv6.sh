#!bash
############################################################
# Squid Proxy Installer
############################################################

if [ `whoami` != root ]; then
	echo "ERROR: Ban can chay script bang root hoac USER co quyen sudo."
	exit 1
fi

## Lấy danh sách các IP hiện có
readarray -t IPv4 <<< $((/sbin/ip -4 -o addr show scope global | awk '{gsub(/\/.*/,"",$4); print $4}') | sed 's/ /\n/g')
readarray -t IPv6 <<< $((/sbin/ip -6 -o addr show scope global | awk '{gsub(/\/.*/,"",$4); print $4}') | sed 's/ /\n/g')

if [ -z "$IPv6" ]
then
    echo "Server chua co IPv6, vui long lien he voi nha cung cap de duoc ho tro"
else
    wget --no-check-certificate -O /usr/local/bin/find-os https://gitlab.com/hungmv/squid-proxy/-/raw/master/find-os.sh >> squid-install.log 2>&1 2>&1
    chmod 755 /usr/local/bin/find-os

    wget --no-check-certificate -O /usr/local/bin/squid-uninstall https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid-uninstall.sh >> squid-install.log 2>&1 2>&1
    chmod 755 /usr/local/bin/squid-uninstall

    wget --no-check-certificate -O /usr/local/bin/squid-add-user https://gitlab.com/hungmv/squid-proxy/-/raw/master/squid-add-user.sh >> squid-install.log 2>&1 2>&1 
    chmod 755 /usr/local/bin/squid-add-user 


    if [[ -d /etc/squid/ || -d /etc/squid3/ ]]; then
        echo "Squid Proxy duoc cai dat. Neu ban muon cai dat lại, truoc tien hay go bo squid proxy bang command: squid-uninstall"
        exit 1
    fi

    if cat /etc/os-release | grep PRETTY_NAME | grep "Ubuntu 22.04"; then
        apt update >> squid-install.log 2>&1
        apt -y install apache2-utils squid >> squid-install.log 2>&1
        touch /etc/squid/passwd >> squid-install.log 2>&1
        mv /etc/squid/squid.conf /etc/squid/squid.conf.bak  >> squid-install.log 2>&1
        touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/squid.conf  >> squid-install.log 2>&1

        service squid restart   >> squid-install.log 2>&1
        systemctl enable squid  >> squid-install.log 2>&1
    elif cat /etc/os-release | grep PRETTY_NAME | grep "Ubuntu 20.04"; then
        apt update >> squid-install.log 2>&1
        apt -y install apache2-utils squid >> squid-install.log 2>&1
        touch /etc/squid/passwd >> squid-install.log 2>&1
        rm -f /etc/squid/squid.conf >> squid-install.log 2>&1
        touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/squid.conf >> squid-install.log 2>&1 
        service squid restart   >> squid-install.log 2>&1
        systemctl enable squid  >> squid-install.log 2>&1
    elif cat /etc/os-release | grep PRETTY_NAME | grep "Ubuntu 18.04"; then
        apt update >> squid-install.log 2>&1
        apt -y install apache2-utils squid3 >> squid-install.log 2>&1
        touch /etc/squid/passwd >> squid-install.log 2>&1
        rm -f /etc/squid/squid.conf >> squid-install.log 2>&1
        touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        #wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/squid.conf >> squid-install.log 2>&1
        
        /sbin/iptables-save >> squid-install.log 2>&1
        service squid restart   >> squid-install.log 2>&1
        systemctl enable squid  >> squid-install.log 2>&1

    elif cat /etc/os-release | grep PRETTY_NAME | grep "jessie"; then
        # OS = Debian 8
        rm -rf /etc/squid  >> squid-install.log 2>&1
        apt update >> squid-install.log 2>&1
        apt -y install apache2-utils squid3 >> squid-install.log 2>&1
        touch /etc/squid3/passwd >> squid-install.log 2>&1
        rm -f /etc/squid3/squid.conf   >> squid-install.log 2>&1
        touch /etc/squid3/blacklist.acl >> squid-install.log 2>&1
        #wget --no-check-certificate -O /etc/squid3/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/squid.conf >> squid-install.log 2>&1
        
        service squid3 restart >> squid-install.log 2>&1
        update-rc.d squid3 defaults >> squid-install.log 2>&1
        ln -s /etc/squid3 /etc/squid >> squid-install.log 2>&1
    elif cat /etc/os-release | grep PRETTY_NAME | grep "stretch"; then
        # OS = Debian 9
        rm -rf /etc/squid >> squid-install.log 2>&1
        apt update >> squid-install.log 2>&1
        apt -y install apache2-utils squid >> squid-install.log 2>&1
        touch /etc/squid/passwd >> squid-install.log 2>&1
        rm -f /etc/squid/squid.conf >> squid-install.log 2>&1
        touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/squid.conf >> squid-install.log 2>&1

        systemctl enable squid  >> squid-install.log 2>&1
        systemctl restart squid >> squid-install.log 2>&1
    elif cat /etc/os-release | grep PRETTY_NAME | grep "buster"; then
        # OS = Debian 10
        rm -rf /etc/squid  >> squid-install.log 2>&1
        apt update >> squid-install.log 2>&1    
        apt -y install apache2-utils squid >> squid-install.log 2>&1
        touch /etc/squid/passwd >> squid-install.log 2>&1
        rm -f /etc/squid/squid.conf >> squid-install.log 2>&1
        touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/squid.conf >> squid-install.log 2>&1

        systemctl enable squid  >> squid-install.log 2>&1
        systemctl restart squid >> squid-install.log 2>&1
    elif cat /etc/os-release | grep PRETTY_NAME | grep "CentOS Linux 7"; then
        yum install squid httpd-tools -y >> squid-install.log 2>&1
        rm -f /etc/squid/squid.conf >> squid-install.log 2>&1
        touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        touch /etc/squid/passwd
        wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/squid.conf >> squid-install.log 2>&1 2>&1
        sed -i 's#/usr/lib/squid3/basic_ncsa_auth#/usr/lib64/squid/basic_ncsa_auth#g' /etc/squid/squid.conf
        systemctl enable squid >> squid-install.log 2>&1 2>&1
        systemctl restart squid >> squid-install.log 2>&1
        #firewall-cmd --zone=public --permanent --add-port=3128/tcp
        #firewall-cmd --reload
    elif cat /etc/os-release | grep PRETTY_NAME | grep "CentOS Linux 8"; then
        yum install squid httpd-tools -y >> squid-install.log 2>&1
        rm -f /etc/squid/squid.conf >> squid-install.log 2>&1
        touch /etc/squid/passwd
        touch /etc/squid/blacklist.acl >> squid-install.log 2>&1
        wget --no-check-certificate -O /etc/squid/squid.conf https://gitlab.com/hungmv/squid-proxy/-/raw/master/conf/squid.conf >> squid-install.log 2>&1
        systemctl enable squid >> squid-install.log 2>&1
        systemctl restart squid >> squid-install.log 2>&1

    else
        echo "Hệ điều hành không được hỗ trợ.\n"
        echo ""
        exit 1;
    fi
    USER_FILE="/root/users.txt"
    > "$USER_FILE"

    PROXY_CONFIG="\n"
    PORT=3200 
    PROXY_CONFIG+="http_port $IPv4:$PORT\n"

    for IP in "${IPv6[@]}"; do

        USER=$(head /dev/urandom | tr -dc a-z0-9 | head -c 10 ; echo '')
        PASSWORD=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 16 ; echo '')
        htpasswd -b /etc/squid/passwd "$USER" "$PASSWORD" >> squid-install.log 2>&1

        echo "$USER/$PASSWORD/$IPv4/$PORT/$IP" >> "$USER_FILE"

        PROXY_CONFIG+="acl acl_${PORT} localport $PORT\n"
        PROXY_CONFIG+="tcp_outgoing_address ${IP} acl_${PORT}\n\n"
        ((PORT++))
        # PROXY_CONFIG+="acl acl_${USER} proxy_auth ${USER}\n"
        # PROXY_CONFIG+="tcp_outgoing_address ${IP} acl_${USER}\n\n"
    done

    echo -e $PROXY_CONFIG > /etc/squid/acl_ipv6.conf
    # iptables-save >> squid-install.log 2>&1

    echo "Restart squid..." >> squid-install.log 2>&1

    systemctl restart squid >> squid-install.log 2>&1


    echo
    echo "Xem thong tin dang nhap Squid proxy tai file /root/users.txt"
    echo 
    echo "Xem huong dan tai https://gitlab.com/hungmv/squid-proxy"
fi
