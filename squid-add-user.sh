#!/bin/bash
############################################################


if [ `whoami` != root ]; then
	echo "ERROR: You need to run the script as user root or add sudo before command."
	exit 1
fi

if [ ! -f /usr/bin/htpasswd ]; then
    echo "htpasswd not found"
    exit 1
fi

read -e -p "Enter Proxy username: " proxy_username

if [ -f /etc/squid/passwd ]; then
    /usr/bin/htpasswd /etc/squid/passwd $proxy_username
else
    /usr/bin/htpasswd -c /etc/squid/passwd $proxy_username
fi

if [ ! -f /usr/local/bin/find-os ]; then
    echo "/usr/local/bin/find-os not found"
    exit 1
fi

OS=$(/usr/local/bin/find-os)

if [ $OS == "ubuntu2204" ]; then
    systemctl reload squid
elif [ $OS == "ubuntu2004" ]; then
    systemctl reload squid
elif [ $OS == "ubuntu1804" ]; then
    systemctl reload squid
elif [ $OS == "ubuntu1604" ]; then
    service squid restart
elif [ $OS == "ubuntu1404" ]; then
    service squid3 restart
elif [ $OS == "debian8" ]; then
    service squid3 restart
elif [ $OS == "debian9" ]; then
    systemctl reload squid
elif [ $OS == "debian10" ]; then
    systemctl reload squid
elif [ $OS == "centos7" ]; then
    systemctl reload squid
elif [ $OS == "centos8" ]; then
    systemctl reload squid
else
    echo "OS NOT SUPPORTED.\n"
    exit 1;
fi
