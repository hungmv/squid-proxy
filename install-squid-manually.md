# Hướng dẫn cài đặt Squid proxy với IPv6 trên VPS
Squid là một ứng dụng máy chủ proxy web đầy đủ tính năng, cung cấp các dịch vụ proxy và cache cho Giao thức HTTP,FTP và các giao thức mạng phổ biến khác. Squid có thể triển khai bộ nhớ đệm và ủy quyền cho các yêu cầu Lớp cổng bảo mật (SSL) cũng như bộ nhớ đệm tra cứu Máy chủ tên miền (DNS) và thực hiện bộ nhớ đệm trong suốt. Squid cũng hỗ trợ nhiều giao thức bộ đệm khác nhau, chẳng hạn như Giao thức bộ đệm Internet (ICP), Giao thức bộ đệm siêu văn bản (HTCP), Giao thức định tuyến mảng bộ đệm (CARP) và Giao thức điều phối bộ đệm web (WCCP).
Trong bài viết này, mình sẽ hướng dẫn bạn cách cài đặt Squid Proxy với IPv6 trên Ubuntu 20.04 và cấu hình sử dụng trình duyệt web FireFox để kết nối

### Nếu bạn chưa có VPS hãy tham khảo các gói VPS của Vinhahost tại đây [VPS SSD](https://vinahost.vn/thue-vps-ssd-chuyen-nghiep/)
## Cài đặt:

Squid package có sẵn trên repository Ubuntu 20.04. Để cài đặt nó, bạn có thể chạy câu lệnh bên dưới:

```
sudo apt update
sudo apt install squid apache2-utils -y
```
Sau khi cài đặt thành công, Squid sẽ được chạy tự động. Bạn có thể kiểm tra Squid đã chạy đúng chưa bằng lệnh:

```
sudo systemctl status squid
``` 
<img src="./img/status.png" alt="img status">

Nếu Squid proxy chưa running bạn có thể bật Squid và kiểm tra lại trang thái của Squid:
```
sudo systemctl start squid
```

Kiểm tra VPS đã có IPv6 chưa bằng lệnh
```
ip addr show
```
<img src="./img/addr.png" alt="img checkaddr">

Như hình trên, IPv6 trên VPS của mình là: 2001:df7:7e80:100:9f2b:1d15:19eb:7a18

Chúng ta sẽ câu hình Squid proxy với IP này

## Bắt đầu cấu hình Squid proxy 

Squid service có thể được cấu hình bằng cách chỉnh sửa file có đường dẫn sau đây: /etc/squid/squid.conf

Trước khi thực hiện thay đổi, mình khuyên bạn nên backup file cấu hình gốc trước khi thực hiện.
```
sudo cp /etc/squid/squid.conf{,.default}
```
Để bắt đầu cấu hình squid, mở file cấu hình lên bằng trình soạn thảo mà bạn sử dụng, ở đây mình dùng nano
```
nano /etc/squid/squid.conf
```
Sau khi mở file, copy và paste đoạn cấu hình sau vào đầu file và lưu lại

```
## Authentication
auth_param basic program /usr/lib/squid3/basic_ncsa_auth /etc/squid/passwd

auth_param basic children 5
auth_param basic realm Squid proxy-caching web server
auth_param basic credentialsttl 2 hours
acl password proxy_auth REQUIRED
http_access allow password
http_access deny all


forwarded_for off

### Allow request header

request_header_access Allow allow all
request_header_access Authorization allow all
request_header_access WWW-Authenticate allow all
request_header_access Proxy-Authorization allow all
request_header_access Proxy-Authenticate allow all
request_header_access Cache-Control allow all
request_header_access Content-Encoding allow all
request_header_access Content-Length allow all
request_header_access Content-Type allow all
request_header_access Date allow all
request_header_access Expires allow all
request_header_access Host allow all
request_header_access If-Modified-Since allow all
request_header_access Last-Modified allow all
request_header_access Location allow all
request_header_access Pragma allow all
request_header_access Accept allow all
request_header_access Accept-Charset allow all
request_header_access Accept-Encoding allow all
request_header_access Accept-Language allow all
request_header_access Content-Language allow all
request_header_access Mime-Version allow all
request_header_access Retry-After allow all
request_header_access Title allow all
request_header_access Connection allow all
request_header_access Proxy-Connection allow all
request_header_access User-Agent allow all
request_header_access Cookie allow all
request_header_access All deny all
```

Mặc định, squid lắng nghe trên port 3128 trên tất cả interfaces trên server.
Nếu bạn muốn thay đổi port và thiết lập một interface listening, mở file /etc/squid/squid.conf sửa dòng  **http_port 3128** và chỉ ra địa chỉ IP của interface và port. Nếu không có interface nào được chỉ ra trong squid thì nó sẽ lắng nghe trên tất cả các interfaces.

Ví dụ, thay đổi port mặc định thành 3333 và lằng nghe trên địa chỉ IP 103.126.161.10
```
# Squid normally listens to port 3128
http_port 103.126.161.10:3333 
```
Tiếp theo ta thêm đoạn cấu hình dưới vào file /etc/squid/squid.conf để Squid proxy với IPv6
```
acl proxy_ipv6  myip 2001:df7:7e80:100:9f2b:1d15:19eb:7a18
tcp_outgoing_address 103.126.161.10 proxy_ipv6
```
**LƯU Ý**: Thay đổi các địa chỉ IPv6 và IPv4 bằng IP của VPS đang cài đặt 

Sau khi hoàn tất việc chỉnh sửa file cấu hình, cần khởi động lại Squid để cấu hình mới có hiệu lực

```
sudo systemctl restart squid
```
Tiếp theo ta bắt đầu tạo user để kết nối

## Tạo user

Trước khi tạo user ta tạo file chứa thông tin xác thực
```
touch /etc/squid/passwd
```
Tiếp theo ta tạo user bằng lệnh sau
```
sudo htpasswd -b -m /etc/squid/passwd vinahost Password
```
Để cập nhật mật khẩu cho người dùng hiện tại, chạy
```
sudo htpasswd /etc/squid/passwd vinahost
```
thay thế vinahost và Password bằng tên người dùng và mật khẩu bạn muốn đặt.


Nếu bạn có tường lửa, bạn cần mở port để có thể kết nối từ bên ngoài.
Ví dụ với tường lửa UFW của ubuntu:
```
sudo ufw allow 3333/tcp
sudo ufw reload
```
## Cấu hình trình duyệt sử dụng proxy 

Hướng dẫn dưới đây mình sử dụng trình duyệt Firefox để kết nối.

Bạn có thể thực các thao tác bên dưới cho các hệ điều hành khác nhau như: Windows, MacOS, Linux.
Bước 1: Phía bên trên bên phải, click vào icon có 3 dấu gạch ngang ☰ để mở menu trình duyệt FireFox lên.

Bước 2: Click vào ⚙ Preferences

Bước 3: Kéo xuống tới phần Network Settings và click vào nút Setting

Bước 4: Một cửa sổ sẽ được mở ra

Chọn vào option Manual proxy configuration
Nhập vào địa chỉ IP của Squid Proxy trong trường HTTP PORT và 3333 ở trường port.
Tích chọn vào ô “Use this proxy server for all protocols“
Click vào nút OK để lưu lại các thiết lập

<img src="./img/setting.png" >

Tiếp theo nhập user và password để xác thực
<img src="./img/auth.png">

Kiểm tra kết quả, truy cập https://whatismyv6.com/ kiểm tra địa chỉ IPv6
<img src="./img/checkipv6.png">

Như vậy mình đã xong phần hướng dẫn cách cài đặt và cấu hình Squid Proxy trên Ubuntu 20.04. Chúc các bạn thành công!